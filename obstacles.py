from config import *
import random


class FixedObstacles(pygame.sprite.Sprite):
    def __init__(self, y_pos):
        super().__init__()
        self.surf = pygame.image.load('spr_ape_yeti.png')  # Image of sprite
        self.surf = pygame.transform.\
            scale(self.surf, (player_width + 20, player_height + 20))
        self.rect = self.surf.get_rect().\
            move(random.randint(0, width - 70), y_pos)  # Set position as
        # specified


class MovingObstacles(pygame.sprite.Sprite):
    def __init__(self, x_pos, y_pos, sp):
        super().__init__()
        self.surf = pygame.image.load('boss_bee.png')
        self.surf = pygame.transform.scale(self.surf, (55, 55))
        self.rect = self.surf.get_rect().move(x_pos, y_pos)
        self.speed = (sp, 0)

    def update(self):
        self.rect = self.rect.move(self.speed)  # After each time the
        # update is called, check if position is still valid
        if self.rect.left >= width:  # If not valid, then remove
            # itself from all groups created
            all_characters.remove(self)
            all_enemy.remove(self)
            all_moving_enemy.remove(self)
