import pygame
pygame.init()

''' Critical parameters for the game '''
screen_size = width, height = (550, 650)
player_size = player_height, player_width = (50, 50)

''' Data needed for sprites '''
moving_speed = [2, 2]
number_per_spawn = 2
time_to_spawn = 2000
minimum_fixed_obstacles = 5
rounds_won = [0, 0]

''' Data required for players and for each round '''
score1 = 0
score2 = 0
current_round = 0
running = True
game = True

''' Groups and sprites '''
all_characters = pygame.sprite.Group()
dead_characters = pygame.sprite.Group()
all_enemy = pygame.sprite.Group()
all_fixed_enemy = pygame.sprite.Group()
all_moving_enemy = pygame.sprite.Group()

''' Generate platform and river surfaces '''
player_surface = [pygame.Surface((width, 50)) for i in range(6)]
region_surface = [pygame.Surface((width, 70)) for j in range(5)]

''' Font '''
my_font = pygame.font.SysFont('orecrusher.ttf', 30)

''' Colors '''
black = (0, 0, 0)
platform = (127, 255, 90)
water = (91, 91, 129)
white = (255, 255, 255)
blue = (66, 114, 245)

''' String to be shown '''
round_over = 'Round Over'  # Note that a lot of strings are dynamic
# in nature and cannot be stored in a config file

''' Custom events as defined '''
ADDENEMY = pygame.USEREVENT + 1  # Adds enemies when triggered every 250 ms
TIME_ALIVE = pygame.USEREVENT + 2  # Increments score by 1 for every
# second alive
