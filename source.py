from obstacles import *
from config import *


class Player(pygame.sprite.Sprite):  # Player class that inherits from Sprite
    def __init__(self, y_pos, s1):
        super().__init__()
        self.surf = pygame.image.load('hat-guy.png')
        self.surf = pygame.transform.scale(self.surf, player_size)
        self.rect = self.surf.get_rect().\
            move(random.randint(0, width - player_width), y_pos)
        self.score = s1  # For keeping track of player score in round
        self.level_score = 0  # To keep track of total score for that player

    def update(self, up, down, left, right):
        to_add = False   # A flag to determine whether to add scores or not
        # depending on collision
        if self.rect.top - 60 >= 0 and up:  # The following lines
            # determine where the player moves
            self.rect = self.rect.move(0, -60)
            to_add = True
        if down and self.rect.bottom + 60 <= height:
            self.rect = self.rect.move(0, 60)
            to_add = True
        if left and self.rect.left >= 0:
            self.rect = self.rect.move(-4, 0)
        if right and self.rect.right <= width:
            self.rect = self.rect.move(4, 0)

        if pygame.sprite.spritecollideany(self, all_moving_enemy):
            print('Collision')  # Kill the player if collision occurs
            self.die()
            return

        if to_add:  # If the player passes the moving obstacles, add 10 points
            self.level_score += 10

        to_add = False

        if up and self.rect.top - 60 >= 0:  # After checking through the
            # middle of the river, go forward
            self.rect = self.rect.move(0, -60)
            to_add = True
        if down and self.rect.bottom + 60 <= height:
            self.rect = self.rect.move(0, 60)
            to_add = True

        if pygame.sprite.spritecollideany(self, all_enemy):  # Check if the
            # player has collided with any fixed obstacles
            print('Collision')
            self.die()
            to_add = False

        if to_add:
            self.level_score += 5  # If successfully jumped to another
            # platform, increment 5 points

    def collide(self, target):  # Standard collision checking function
        hitbox = self.rect.inflate(-10, -10)
        return hitbox.colliderect(target.rect)

    def die(self):  # Procedure that is called after player death
        self.kill()
        self.surf = pygame.image.load('skull.png')  # Show skull image on die
        self.surf = pygame.transform.scale(self.surf, player_size)
        dead_characters.add(self)  # dead player --> dead characters group


def create_fixed_obstacles(m):  # Function that handles creation of
    # fixed obstacles
    for q in all_fixed_enemy:  # Kill any existing fixed obstacles
        q.kill()

    while len(all_fixed_enemy) != m:  # Create a minimum number of fixed
        # obstacles denoted by m
        f_obstacle = FixedObstacles(random.randint(0, 6) * 120 - 20)
        # Randomize location
        if not pygame.sprite.spritecollideany(f_obstacle, all_characters):
            all_characters.add(f_obstacle)  # Add sprite only if valid
            all_enemy.add(f_obstacle)
            all_fixed_enemy.add(f_obstacle)


def create_moving_obstacles(sp, q):  # sp = speed, q = number per spawn
    for riv in range(6):  # Loop to spawn on all rivers
        for j in range(random.randint(0, q)):  # Randomize the number of spawns
            m_obstacle = \
                MovingObstacles(random.randint(-400, 0), riv * 120 + 50, sp)
            all_characters.add(m_obstacle)
            all_enemy.add(m_obstacle)
            all_moving_enemy.add(m_obstacle)


def update_tiles():  # Create the map
    level = 0
    for t in player_surface:  # Creates platform
        screen.blit(t, t.fill(platform).move(0, level * 120))
        level += 1
    level = 0
    for r in region_surface:  # creates the river area
        screen.blit(r, r.fill(water).move(0, level * 120 + 50))
        level += 1


def initialize(s1, s2):  # Deals with creation of players,
    # taking in their scores
    p1 = Player(600, s1)
    p2 = Player(0, s2)
    all_characters.add(p1)
    all_characters.add(p2)
    return p1, p2  # Returns the created players


def display_round_over():
    r = True
    st = pygame.time.get_ticks()
    while r:
        for events in pygame.event.get():
            if events.type == pygame.QUIT:
                r = False
                exit(0)
            if events.type == pygame.KEYDOWN:
                if events.key == pygame.K_SPACE:
                    r = False
                if events.key == pygame.K_ESCAPE:
                    exit(0)
        sec = (pygame.time.get_ticks() - st) / 1000  # calculate seconds
        if sec > 2:
            r = False
        ts = my_font.render(round_over, False, (255, 0, 0))
        pygame.display.update()
        pygame.draw.rect(screen, (255, 255, 0), (0, 275, 130, 100))
        screen.blit(ts, (0, 315))


pygame.init()
screen = pygame.display.set_mode(screen_size)

pygame.time.set_timer(ADDENEMY, 2000)  # Set custom events & trigger parameter
pygame.time.set_timer(TIME_ALIVE, 1000)

while game:

    current_round += 1

    player1, player2 = initialize(score1, score2)  # Create the two players
    for i in all_moving_enemy:  # Kill moving enemies so we spawn them later
        i.kill()
    create_fixed_obstacles(minimum_fixed_obstacles)
    create_moving_obstacles(moving_speed[0], number_per_spawn)
    pygame.time.set_timer(ADDENEMY, time_to_spawn)

    running = True  # PLAYER 1
    while running:
        key_pressed = pygame.key.get_pressed()  # moves the players
        if key_pressed[pygame.K_LEFT]:  # We need smooth horizontal movement
            player1.update(0, 0, 1, 0)
        elif key_pressed[pygame.K_RIGHT]:
            player1.update(0, 0, 0, 1)

        for events in pygame.event.get():
            if events.type == pygame.QUIT:
                print('attempting to quit')
                exit(0)
            if events.type == pygame.KEYDOWN:  # Restricts vertical movement
                # to between platforms
                if events.key == pygame.K_UP:
                    player1.update(1, 0, 0, 0)
                elif events.key == pygame.K_DOWN:
                    player1.update(0, 1, 0, 0)
            elif events.type == ADDENEMY:  # Spawn more obstacles
                create_moving_obstacles(moving_speed[0], number_per_spawn)
            if events.type == TIME_ALIVE:
                player1.level_score += 1

        screen.fill(white)
        if player1 not in all_characters or player1.rect[1] == 0:
            print('quitting here')
            running = False

        update_tiles()

        for m_o in all_moving_enemy:  # Moving obstacles
            m_o.update()

        for ch in all_characters:  # Display all characters
            screen.blit(ch.surf, ch.rect)

        text_surface = my_font.render('Player 1: ' +
                                      str(player1.level_score), False, black)
        screen.blit(text_surface, (0, 0))
        pygame.display.flip()
        pygame.time.delay(15)

    score1 = player1.score + player1.level_score  # update scores, total scores
    player1.score += player1.level_score
    print('Moving onto player 2')

    # PLAYER 2
    running = True  # Do something similar for player 2
    while running:
        key_pressed = pygame.key.get_pressed()
        if key_pressed[pygame.K_a]:  # Left
            player2.update(0, 0, 1, 0)
        elif key_pressed[pygame.K_d]:  # Right
            player2.update(0, 0, 0, 1)

        for events in pygame.event.get():
            if events.type == pygame.QUIT:
                print('attempting to quit')
                exit(0)
            if events.type == pygame.KEYDOWN:
                if events.key == pygame.K_w:
                    player2.update(1, 0, 0, 0)
                elif events.key == pygame.K_s:
                    player2.update(0, 1, 0, 0)
            elif events.type == ADDENEMY:
                create_moving_obstacles(moving_speed[1], number_per_spawn)
            if events.type == TIME_ALIVE:
                player2.level_score += 1

        screen.fill((255, 255, 255))
        if player2 not in all_characters or player2.rect[1] == 600:
            running = False

        update_tiles()
        for d in dead_characters:
            screen.blit(d.surf, d.rect)
        for m_o in all_moving_enemy:
            m_o.update()

        for ch in all_characters:
            screen.blit(ch.surf, ch.rect)

        text_surface = my_font.\
            render('Player 2: ' + str(player2.level_score), False, (0, 0, 0))
        screen.blit(text_surface, (0, 0))
        pygame.display.flip()
        pygame.time.delay(15)

    display_round_over()  # Indicating the round is over after both
    # players' turns are over

    running = True
    start_ticks = pygame.time.get_ticks()
    temp_time = 5
    while running:
        seconds = (pygame.time.get_ticks() - start_ticks) / 1000  # calculate
        # how many seconds
        if seconds > 2:  # if more than 10 seconds close the game
            running = False

    score2 = player2.score + player2.level_score
    player2.score += player2.level_score

    p = ''  # Create the display strings
    if (player1 in all_characters and player2 in all_characters) or \
            (player1 not in all_characters and player2 not in all_characters):
        # Check which player won
        if player1.level_score < player2.level_score:
            p = 'Player 1 wins!'
            rounds_won[0] += 1  # Update rounds won
            moving_speed[0] = min(moving_speed[0] + 1, 9)  # Set the moving
            # speeds of each player accordingly for round2
        elif player2.level_score < player1.level_score:
            p = 'Player 2 wins!'
            rounds_won[1] += 1
            moving_speed[1] = min(moving_speed[1] + 1, 9)
        else:
            p = 'Draw!'
    elif player1 not in all_characters:
        p = 'Player 2 wins!'
        rounds_won[1] += 1
        moving_speed[1] = min(moving_speed[1] + 1, 9)
    else:
        p = 'Player 1 wins!'
        rounds_won[0] += 1
        moving_speed[0] = min(moving_speed[0] + 1, 9)

    running = True
    start_ticks = pygame.time.get_ticks()
    while running:  # Just display bunch of data
        for events in pygame.event.get():
            if events.type == pygame.QUIT:
                running = False
                game = False
            if events.type == pygame.KEYDOWN:
                if events.key == pygame.K_SPACE:
                    running = False
                if events.key == pygame.K_ESCAPE:
                    running = False
                    game = False
        seconds = (pygame.time.get_ticks() - start_ticks) / 1000  # calculate
        # how many seconds
        if seconds > 8:  # Display the screen for some time and automatically
            # load to new round
            running = False
        screen.fill((255, 255, 255))
        text_surface0 = my_font.render('Round ' + str(current_round + 1) +
                                       ' is about to start', False, blue)
        screen.blit(text_surface0, (0, 300))
        d = ''
        if player1 not in all_characters:
            d = ' (dead)'

        text_surface1 =\
            my_font.render('Player 1 current: ' +
                           str(player1.level_score) + d, False, (0, 0, 0))
        screen.blit(text_surface1, (0, 0))

        text_surface1 = my_font.\
            render('Player 1 total score: ' +
                   str(player1.score) + d, False, (0, 0, 0))
        screen.blit(text_surface1, (0, 50))

        text_surface1 = \
            my_font.render('Player 1 has won: ' +
                           str(rounds_won[0]) + ' rounds' +
                           d, False, (0, 0, 0))
        screen.blit(text_surface1, (0, 100))

        d = ''
        if player2 not in all_characters:
            d = ' (dead)'

        text_surface = \
            my_font.render('Player 2 current score: ' +
                           str(player2.level_score) + d,
                           False, (0, 221, 255))
        screen.blit(text_surface, (0, 600))

        text_surface = \
            my_font.render('Player 2 total score: ' +
                           str(player2.score) + d,
                           False, (0, 221, 255))
        screen.blit(text_surface, (0, 550))

        text_surface = \
            my_font.render('Player 2 has won ' +
                           str(rounds_won[1]) + ' rounds. ' + d,
                           False, (0, 221, 255))
        screen.blit(text_surface, (0, 500))

        text_surface2 = my_font.render(p, False, (255, 0, 0))
        screen.blit(text_surface2, (300, 300))

        text_surface3 = \
            my_font.render('Press space to continue, escape to quit',
                           False, (192, 251, 95))
        screen.blit(text_surface3, (100, 450))
        pygame.display.flip()

    number_per_spawn = min(4, number_per_spawn + 1)  # Update some parameters
    # for increasing difficulty
    time_to_spawn = max(time_to_spawn - 250, 650)
    minimum_fixed_obstacles = min(minimum_fixed_obstacles + 2, 10)

    player1.kill()  # Reset player 1 and player 2
    player2.kill()
