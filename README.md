# Iss Assignment 3 (2019101014)

A simple game using python3 and the pygame library for ISS Assignment 3

# About the game

There are two players, player one and two, with player one starting from
the bottom and player two starting from the top.
The game is about the players competing with each other, objective being to
cross the partitions and reach the opposite side where the player spawned on.

Each game consists of never ending series of rounds, allowing for flexibility.
At the end of each round, information regarding round score, total score and
total number of rounds won for each player is displayed.

Player 1 uses arrow keys for movement while Player 2 uses WASD

Game is executed by `python3 source.py`

# Design Choices

*  Random position of fixed obstacles
*  Random spawn location and number of moving obstacles
*  Random horizontal spawn of each player
*  Only the current player's score is displayed on the screen
*  In order to prevent the game becoming boring for one of the players, the 
speed of the moving obstacles increase for the player that won the previous
round and not for the other player, but the number of spawned obstacles increases
in a linear fashion for both the players
*  Horizontal movement for each player is smooth but the range of vertical
motion is restricted to allow for jumping between platforms without overshooting
his position
*  Samurai


# Assumption

The only major and reasonable assumption made was in the scoring system; the 
player with the lower score wins.

Justification: 

Each second constitutes +1 point. Higher the points accumulated,
the more time the player has taken to finish the round
    
Each successful pass of obstacle adds to the score which implies that 
the player with the greater score has not been playing efficiently.